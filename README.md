## Giphy Box 

'Giphy Box' Single Page Application for searching and sharing funny gifs with your friends. Consumes "GIPHY API" and visualizes Gif images, based on search criteria or likes, facilitates upload of new gif images and uses basic browser storage. Colaborated with Mitko Mitev.

### Technologies:

- ##### JavaScript, jQuery, HTML, CSS, Sass, ESLint, npm

### Open it at: http://zarmartinov.com/giphy-box