/* eslint-disable max-len */
const switchingBgrBlack = () => {
  $('.container')
      .css('background-color', 'var(--color-gray-dark)')
      .css('color', 'var(--color-gray-light)');
  $('.hero-box').css('border-color', 'var(--color-gray-nav)');
  $('.footer').css('border-color', 'var(--color-gray-nav)');
  $('.container__menu').css('background-color', 'var(--color-gray-nav)');
  $('.column-switching-btns__icon').css('fill', 'var(--color-gray-light)');
};
const switchingBgrWhite = () => {
  $('.container')
      .css('background-color', 'var(--color-gray-light)')
      .css('color', 'var(--color-gray-darken)');
  $('.hero-box').css('border-color', 'var(--color-gray-line)');
  $('.footer').css('border-color', 'var(--color-gray-line)');
  $('.container__menu').css('background-color', 'var(--color-gray-nav-light-theme)');
  $('.column-switching-btns__icon').css('fill', 'var(--color-gray-darken)');
};

export const switchingTheme = (event) => {
  if ($('#switching-bgr').is(':checked')) {
    switchingBgrWhite();
  } else {
    switchingBgrBlack();
  }
};
