const apiKey = 'ZRQ04jwqTbrlCgoajETF9cMQgfSPSfY0';

export const searchGifs = async (queryString) => {
  const res = await fetch(
      `http://api.giphy.com/v1/gifs/search?api_key=${apiKey}&offset=9&q=${queryString}&limit=35`
  );
  const jsonVar = (await res.json()).data;
  return jsonVar;
};

export const getTrendingGifs = async () => {
  const res = await fetch(
      `http://api.giphy.com/v1/gifs/trending?api_key=${apiKey}&limit=100`
  );
  const jsonVar = (await res.json()).data;
  return jsonVar;
};

export const randomGif = async () => {
  const gif = await fetch(
      `http://api.giphy.com/v1/gifs/random?api_key=${apiKey}`
  );
  const jsonVar = (await gif.json()).data;
  return jsonVar;
};

export const getGifById = async (id) => {
  const gif = await fetch(
      `http://api.giphy.com/v1/gifs/${id}?api_key=${apiKey}`
  );
  const jsonVar = (await gif.json()).data;
  return jsonVar;
};

export const uploadGif = async (file) => {
  const formData = new FormData();
  formData.append('file', file);
  const url = `http://upload.giphy.com/v1/gifs?api_key=${apiKey}`;

  const response = await fetch(url, {
    method: 'POST',
    body: formData,
    // mode: 'no-cors'
  });
  const uploadedGifId = (await response.json()).data.id;
  return uploadedGifId;
};
