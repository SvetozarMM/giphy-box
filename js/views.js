/* eslint-disable max-len */
const USER = 'mitko094';

import {
  getGifById,
  getTrendingGifs,
  searchGifs as request,
  uploadGif,
  randomGif,
} from './giphy-service.js';

export const appendHeading = (heading) =>
  $('.heading__first').html(`${heading}`);

const appendingBtnInfo = (id) => {
  // appending btn-info
  $('<div>')
      .attr('class', 'btn-info')
      .attr('id', `btn-info-id${id}`)
      .appendTo(`#gif-box${id}`);
  // info-btn checkbox
  $('<input>')
      .attr('class', 'btn-info__checkbox')
      .attr('type', 'checkbox')
      .attr('id', `btn-checkbox-info${id}`)
      .appendTo(`#btn-info-id${id}`);

  $('<label>')
      .attr('for', `btn-checkbox-info${id}`)
      .attr('class', 'btn-info__btn--open')
      .attr('id', `label-open-info${id}`)
      .appendTo(`#btn-info-id${id}`);
  $('<img>')
      .attr('src', './img/info-large.png')
      .attr('alt', 'fav-img')
      .attr('class', 'btn-info__icon')
      .appendTo(`#label-open-info${id}`);

  $('<label>')
      .attr('for', `btn-checkbox-info${id}`)
      .attr('class', 'btn-info__btn--close')
      .attr('id', `label-checked-info${id}`)
      .appendTo(`#btn-info-id${id}`);
  $('<img>')
      .attr('src', './img/cross.png')
      .attr('alt', 'fav-img')
      .attr('class', 'btn-info__icon')
      .appendTo(`#label-checked-info${id}`);
};

const appendingBtnFav = (id) => {
  const favoriteGifs = JSON.parse(localStorage.getItem('favoriteGifs'));
  // appending btn-fav
  $('<div>')
      .attr('class', 'btn-fav')
      .attr('id', `btn-fav-id${id}`)
      .appendTo(`#gif-box${id}`);
  // favorite-btn checkbox
  $('<input>')
      .attr('class', 'btn-fav__checkbox')
      .attr('type', 'checkbox')
      .attr('checked', favoriteGifs.includes(id) ? '' : null)
      .attr('id', `btn-checkbox${id}`)
      .appendTo(`#btn-fav-id${id}`);

  $('<label>')
      .attr('for', `btn-checkbox${id}`)
      .attr('class', 'btn-fav__btn--open')
      .attr('id', `label-open${id}`)
      .appendTo(`#btn-fav-id${id}`);
  $('<img>')
      .attr('src', './img/heart_empty.png')
      .attr('alt', 'fav-img')
      .attr('class', 'btn-fav__icon')
      .appendTo(`#label-open${id}`);

  $('<label>')
      .attr('for', `btn-checkbox${id}`)
      .attr('class', 'btn-fav__btn--close')
      .attr('id', `label-checked${id}`)
      .appendTo(`#btn-fav-id${id}`);
  $('<img>')
      .attr('src', './img/heart_full.png')
      .attr('alt', 'fav-img')
      .attr('class', 'btn-fav__icon')
      .appendTo(`#label-checked${id}`);
};

const appendItem = (url, id) => {
  $('<div>')
      .attr('class', 'hero-box__item')
      .attr('id', `gif-box${id}`)
      .appendTo('#hero');
  $('<img>')
      .attr('class', 'hero-box__img')
      .attr('src', url)
      .attr('id', `gif-img${id}`)
      .attr('alt', 'gif')
      .appendTo(`#gif-box${id}`);

  appendingBtnFav(id);
  appendingBtnInfo(id);
  return $(`#gif-box${id}`);
};

const appendBtnRemove = (id) => {
  $('<div>')
      .attr('class', 'btn-uploaded')
      .attr('id', `btn-uploaded-id${id}`)
      .appendTo(`#gif-box${id}`);
  $('<img>')
      .attr('src', './img/cloud.png')
      .attr('alt', 'upload-img')
      .attr('class', 'btn-uploaded__icon btn-uploaded__icon--upload')
      .appendTo(`#btn-uploaded-id${id}`);
};

const appendItemMeta = (id, title, username, source, dateImport) => {
  $('<div>')
      .attr('class', 'hero-box__meta')
      .attr('id', `hero-box__meta-id${id}`)
      .html(
          `Title: ${title}<br>User: ${username}<br>Source: ${source}<br>Import date: ${dateImport}`
      )
      .appendTo(`#gif-box${id}`);
};

export const displayGifInfo = async (event) => {
  const gifId = event.target.id;
  const gifIdClear = gifId.replace(/btn-checkbox-info/i, '');

  if ($(`#${gifId}`).is(':checked')) {
    const gifInfo = await getGifById(gifIdClear);
    appendItemMeta(
        gifIdClear,
        gifInfo.title || 'No title provided',
        gifInfo.username || 'unknown',
        gifInfo.source_tld || 'unknown',
        gifInfo.import_datetime || 'No date recorded'
    );
  } else {
    $(`#hero-box__meta-id${gifIdClear}`).remove();
  }
};

export const displayGifs = async (container, gifs) => {
  $(container).empty();
  if (!gifs || (gifs && !gifs.length)) {
    $('<h2>')
        .attr('class', 'heading heading__second')
        .html('No gifs found!')
        .appendTo(`#hero`);
    return;
  }
  // array containing JQuery collection of the appendedGifs
  const appendedGifs = await Promise.all(
      gifs.map((element, i) => {
        const url = './assets/loading1.gif';
        // console.log(element.id);
        const $appendedGif = appendItem(url, element.id);
        // get uploaded items
        if (element.username === USER) {
          appendBtnRemove(element.id);
        }
        // console.log("Loading gif", element);
        const lowQuality = new Promise((res, rej) => {
          setTimeout(() => {
          // V this executes the first time it loads
            $appendedGif
                .children()
                .first()
                .attr('src', element.images.fixed_width_downsampled.url);
            $appendedGif.children().on('load', async (ev) => {
              res({
                htmlElement: $appendedGif,
                originalUrl: element.images.original.url,
              });
            });
          // console.log("Downsized quality", $appendedGif);
          }, 50);
        // at least 50 ms before each element starts loading
        });
        return lowQuality;
      })
  );
  appendedGifs.forEach(({ htmlElement: $gifDiv, originalUrl: url }) => {
    $($gifDiv)
        .children()
        .attr('src', url);
  });
  // eslint-disable-next-line consistent-return
  return appendedGifs;
};

export const searchGifs = async (event) => {
  const searchString = $('#form-value').val();
  $('#hero').attr('gif-type', 'searching');
  if (!searchString) {
    return;
  }
  const foundGifs = await request(searchString);
  appendHeading(`Searched '${searchString}' GIFs`);
  displayGifs('#hero', foundGifs);
  $('#form-value').val('');
};

export const trending = async (event, of, lim) => {
  const trendingGifys = await getTrendingGifs(of, lim);
  appendHeading('Trending GIFs');
  displayGifs('#hero', trendingGifys);
  $('#hero').attr('gif-type', 'trending');
};

// add functionality to multiply default limit * number of page and add it to offset
export const switchFav = async (event) => {
  const gifId = $(event.target)
      .parent()
      .attr('id')
      .replace(/label-open/i, '') // label-checked
      .replace(/label-checked/i, '');
  const gifType = $('#hero').attr('gif-type');
  if (!addFavGif(gifId) && gifType === 'favorites') {
    $(event.target)
        .parent()
        .parent()
        .parent()
        .hide('fast', function() {
          // eslint-disable-next-line no-invalid-this
          this.remove();
        });
    if (!JSON.parse(localStorage.getItem('favoriteGifs')).length && gifId) {
      appendHeading('No favorite gifs selected, choosing random gif');
      const gif = await randomGif();

      localStorage.setItem('favoriteGifs', JSON.stringify([gif.id]));
      // displayLoadingGifs("#hero", [gif]);
      // ^ insert loading gif here
      const $displayedGifs = await displayGifs('#hero', [gif]);
      appendHeading('Visualised random gif');
    }
  }
  // ^ removes gif in a slick way from favorites
};
export const addFavGif = (id) => {
  let arr = JSON.parse(localStorage.getItem('favoriteGifs'));
  if (arr.includes(id)) {
    arr = arr.filter((el) => el !== id);
    localStorage.setItem('favoriteGifs', JSON.stringify(arr));
    return false;
  }
  arr.push(id);
  localStorage.setItem('favoriteGifs', JSON.stringify(arr));
  return true;
};

// additional functionality
export const displayFavGifs = async (event) => {
  appendHeading('Favorite GIFs');
  const gifIds = JSON.parse(localStorage.getItem('favoriteGifs'));
  let gifs;

  // console.log(gifIds.includes($(event.target).attr("gif-id")));
  if (!gifIds.length || gifIds.includes($(event.target).attr('gif-id'))) {
    const gif = await randomGif();
    localStorage.setItem('favoriteGifs', JSON.stringify([gif.id]));
    gifs = [gif];
    appendHeading('No favorite gifs selected, choosing random gif');
  } else {
    gifs = await Promise.all(gifIds.map((id) => getGifById(id)));
  }
  // console.log(gifIds.length);
  displayGifs('#hero', gifs);
  $('#hero').attr('gif-type', 'favorites');
};

export const onHoverLoadGif = async (event) => {
  const gifId = $(event.target)
      .parent()
      .attr('gif-id');
  if (!gifId || $(event.target).attr('loaded')) {
    // ^ makes the onhoveer event only work once and not reload gif everytime;;
    return;
  }
  const gif = await getGifById(gifId);
  $(event.target).attr('src', gif.images.original.url);
  $(event.target).attr('loaded', true);
};

export const uploadingFile = async (event) => {
  // console.log(event.target.files[0]);
  const uploadedGifId = await uploadGif(event.target.files[0]);
  console.log(uploadedGifId);
  addUploadGif(uploadedGifId);
};

const addUploadGif = (id) => {
  $('#uploadedResponse').html('Gif Uploaded!');
  setTimeout(() => {
    $('#uploadedResponse').html('');
  }, 2000);

  let arr = JSON.parse(localStorage.getItem('uploadedGifs'));
  if (arr.includes(id)) {
    arr = arr.filter((el) => el !== id);
    localStorage.setItem('uploadedGifs', JSON.stringify(arr));
    return false;
  }
  arr.push(id);
  localStorage.setItem('uploadedGifs', JSON.stringify(arr));
  return true;
};

export const displayUploadGifs = async (event) => {
  const gifIds = JSON.parse(localStorage.getItem('uploadedGifs'));
  // console.log(gifIds);
  if (gifIds) {
    appendHeading('Uploaded GIFs');
    const gifs = await Promise.all(gifIds.map((id) => getGifById(id)));
    displayGifs('#hero', gifs);
    $('#hero').attr('gif-type', 'uploaded');
  } else {
    displayGifs('#hero');
    appendHeading('No uploaded gifs');
  }
};
//  column switching
export const switchColumnOne = (event) => {
  $('.hero-box').css('column-count', '1');
};
export const switchColumnTwo = (event) => {
  $('.hero-box').css('column-count', '2');
};
export const switchColumnThree = (event) => {
  $('.hero-box').css('column-count', '3');
};
export const switchColumnFour = (event) => {
  $('.hero-box').css('column-count', '4');
};
