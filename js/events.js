export const searchBtn = (callback) => {
  $(document).on('click', '#search-btn', callback);
  $(document).on('change', '#form-value', callback);
  // on input
  // $(document).on('input', '.search-input', callback);
};
export const trendingBtn = (callback) =>
  $(document).on('click', '#trending-btn', callback);
export const favBtn = (callback) =>
  $(document).on('click', '#favorite-btn', callback);
export const selectFavGif = (callback) =>
  $(document).on('click', '.btn-fav__icon', callback);
export const hoverLoadGif = (callback) =>
  $(document).on('mouseover', '#hero', callback);
export const btnInfoChecked = (callback) =>
  $(document).on('click', '.btn-info__checkbox', callback);
export const clickToUpload = (callback) =>
  $(document).on('change', '#file-upload-id', callback);
export const uploadedBtn = (callback) =>
  $(document).on('click', '#uploaded-btn', callback);
export const btnSwitchBgr = (callback) =>
  $(document).on('click', '#switching-bgr', callback);
// column switching
export const btnSwitchColumnOne = (callback) =>
  $(document).on('click', '#column-switching-one', callback);
export const btnSwitchColumnTwo = (callback) =>
  $(document).on('click', '#column-switching-two', callback);
export const btnSwitchColumnThree = (callback) =>
  $(document).on('click', '#column-switching-three', callback);
export const btnSwitchColumnFour = (callback) =>
  $(document).on('click', '#column-switching-four', callback);

