import { getTrendingGifs, randomGif, getGifById } from './giphy-service.js';
import {
  displayGifs,
  searchGifs,
  trending,
  switchFav,
  onHoverLoadGif,
  displayFavGifs,
  appendHeading,
  displayGifInfo,
  uploadingFile,
  displayUploadGifs,
  switchColumnOne,
  switchColumnTwo,
  switchColumnThree,
  switchColumnFour,
} from './views.js';
import {
  searchBtn,
  trendingBtn,
  selectFavGif,
  hoverLoadGif,
  favBtn,
  btnInfoChecked,
  clickToUpload,
  uploadedBtn,
  btnSwitchBgr,
  btnSwitchColumnOne,
  btnSwitchColumnTwo,
  btnSwitchColumnThree,
  btnSwitchColumnFour,
} from './events.js';
import { switchingTheme } from './styling.js';

// onready
$(async () => {
  if (!localStorage.getItem('favoriteGifs')) {
    localStorage.setItem('favoriteGifs', JSON.stringify([]));
  }
  if (!localStorage.getItem('uploadedGifs')) {
    localStorage.setItem('uploadedGifs', JSON.stringify([]));
  }

  const trendingGifs = await getTrendingGifs();
  appendHeading('Trending GIFs');
  displayGifs('#hero', trendingGifs);
  searchBtn(searchGifs);
  trendingBtn(trending);
  selectFavGif(switchFav);
  hoverLoadGif(onHoverLoadGif);
  favBtn(displayFavGifs);
  btnInfoChecked(displayGifInfo);
  clickToUpload(uploadingFile);
  uploadedBtn(displayUploadGifs);
  btnSwitchBgr(switchingTheme);
  btnSwitchColumnOne(switchColumnOne);
  btnSwitchColumnTwo(switchColumnTwo);
  btnSwitchColumnThree(switchColumnThree);
  btnSwitchColumnFour(switchColumnFour);
});
